# Flappy Mecin

## Deskripsi Aplikasi

Flappy Mecin adalah sebuah permainan di mana *player* akan bermain sebagai sebuah
bungkus mecin yang harus melewati rintangan dalam bentuk DNA. *Player* juga akan
bersaing dengan sebuah *AI* tentang siapa yang bertahan lebih lama. Permainan
berakhir apabila *player* gagal melewati rintangan.
 
## Manual

Untuk mulai memainkan *game* ini, cukup buka `FlappyMecin.exe` pada *folder* ini.
Pada *main menu* terdapat mode `train` dan `VS`.

 `train` dipilih untuk melatih *AI* dalam melewati rintangan yang ada. Gunakan
 opsi `save` untuk menyimpan konfigurasi *brain AI* yang diinginkan. Tombol
 *pause* di pojok kanan atas dapat digunakan apabila ingin menghentikan permainan
 sementara. Gunakan opsi `resume` untuk melanjutkan *training*, `restart` untuk
 mengulang *training* dari awal, dan `menu` untuk kembali ke *main menu*.
 
 > Melatih AI dapat memakan waktu yang cukup lama, umumnya sampai puluhan generasi.
 
 `VS` dipilih untuk bermain melawan *AI*. Terdapat 4 *difficulty modes*, yakni
 `easy`, `medium`, `hard`, dan `custom`. *Mode* `custom` akan menggunakan
 konfigurasi *brain AI* yang telah disimpan dari mode `train` sehingga tidak bisa
 dimainkan apabila belum pernah menyimpan konfigurasi *brain AI* sebelumnya.
 
 Saat melawan *AI*, *player* akan bermain dengan mecin berwarna
 <span style="color:red">merah</span>, sementara *AI* akan menggunakan mecin berwarna
 <span style="color:green">hijau</span>. Terdapat 3 cara untuk melakukan aksi lompat:

 1. <kbd>Left-click</kbd> pada *mouse* atau *touchpad*
 2. Menekan tombol <kbd>Enter</kbd> pada *keyboard*
 3. Menekan tombol <kbd>Space</kbd> pada *keyboard*
 
 *Player* dianggap menang apabila *AI* menyentuh rintangan, *ceiling*, atau *ground*
 lebih dahulu daripada *player*. *Player* dianggap kalah apabila menyentuh rintangan,
 *ceiling*, atau *ground* lebih dulu daripada *AI*. Skor *player* akan ditampilkan
 selama permainan berlangsung.
 
Tombol *pause* di pojok kanan atas dapat digunakan apabila ingin menghentikan
permainan sementara. Gunakan opsi `resume` untuk melanjutkan *permainan*, `restart`
untuk mengulang *permainan* dari awal, dan `menu` untuk kembali ke *main menu*.
 
## Resources

- [Menu music](https://freesound.org/people/Creeper_Ciller78/sounds/346895/?page=2#comment)
- [Mecin shaking SFX](https://freesound.org/people/simosco/sounds/235561/)
- [Score SFX](https://freesound.org/people/FunWithSound/sounds/456965/)
- [Collision SFX](https://freesound.org/people/MATTIX/sounds/459150/)
- [Win SFX](https://freesound.org/people/alanmcki/sounds/400579/)
- [Lose SFX](https://freesound.org/people/stumpbutt/sounds/381770/)
- Semua aset *sprite* dibuat oleh [Alya Zahra](https://gitlab.com/alzahra)
  dan [Julia Ningrum](https://gitlab.com/Juliani27).

## Kelompok Mecin Learning

  | No. | Nama                   | NPM        |
  | --- | ---------------------- | ---------- |
  | 1.  | Alya Zahra             | 1706039906 |
  | 2.  | Julia Ningrum          | 1706979322 |
  | 3.  | Sage Muhammad Abdullah | 1706979455 |

## Link Aplikasi  
http://bit.ly/FlappyMecin  