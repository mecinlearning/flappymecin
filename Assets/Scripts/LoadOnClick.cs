﻿using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class LoadOnClick : MonoBehaviour
{
    public void LoadScene(int scene)
    {
        if (scene == 1) {
            AIProperties.savedProperties.Clear();
            AIProperties.generation = 1;
        }
        SceneManager.LoadScene(scene);
    }

    public void LoadVSSceneWithDifficulty(int difficulty) {
        if (difficulty == 0 && !CustomBrainIsAvailable()) {
            TrainMenu();
            return;
        }
        BrainConfig.SetBrainSelection(difficulty);
        LoadScene(2);
    }

    private bool CustomBrainIsAvailable() {
        if (File.Exists(BrainConfig.GetBrainPath(0))) {
            return true;
        }
        return false;
    }

    public void TrainMenu() {
        if (EventSystem.current.currentSelectedGameObject.name.Equals("CustomDifficultyButton")) {
            GameControl.instance.noCustomPanel.SetActive(true);
            ShowDifficulty(false);
            foreach (GameObject button in GameControl.instance.menuButtons) {
                button.gameObject.SetActive(false);
            }
        }
    }

    public void PauseMenu()
    {
        if (Time.timeScale == 0f)
        {
            string activeSceneName = SceneManager.GetActiveScene().name;
            if (activeSceneName.Equals("Train Scene")) {
                Time.timeScale = 2f;
            } else if (activeSceneName.Equals("VS Scene")) {
                Time.timeScale = 1f;
            }
            ShowButtons(false);
            foreach (GameObject confirmation in GameControl.instance.confirmations)
            {
                if (confirmation != null) {
                    confirmation.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            Time.timeScale = 0f;
            ShowButtons(true);
        }
    }

    public void RestartScene()
    {
        if (EventSystem.current.currentSelectedGameObject.name.Equals("RestartButton"))
        {
            GameControl.instance.confirmations[0].SetActive(true);
            ShowButtons(false);
        }
        if (EventSystem.current.currentSelectedGameObject.name.Equals("NoRestart"))
        {
            GameControl.instance.confirmations[0].SetActive(false);
            ShowButtons(true);
        }
        if (EventSystem.current.currentSelectedGameObject.name.Equals("YesRestart"))
        {
            LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void BackToMenu()
    {
        if (EventSystem.current.currentSelectedGameObject.name.Equals("MenuButton"))
        {
            GameControl.instance.confirmations[1].SetActive(true);
            ShowButtons(false);
        }
        if (EventSystem.current.currentSelectedGameObject.name.Equals("NoMenu"))
        {
            GameControl.instance.confirmations[1].SetActive(false);
            ShowButtons(true);
        }
        if (EventSystem.current.currentSelectedGameObject.name.Equals("YesMenu"))
        {
            LoadScene(0);
        }
    }

    public void SaveAI()
    {
        if (EventSystem.current.currentSelectedGameObject.name.Equals("SaveButton"))
        {
            Time.timeScale = 0f;
            GameControl.instance.confirmations[2].SetActive(true);
            ShowButtons(false);
        }
        if (EventSystem.current.currentSelectedGameObject.name.Equals("NoSave"))
        {
            GameControl.instance.confirmations[2].SetActive(false);
            Time.timeScale = 2f;
        }
        if (EventSystem.current.currentSelectedGameObject.name.Equals("YesSave"))
        {
            GameControl.instance.confirmations[2].SetActive(false);
            GameControl.instance.confirmations[3].SetActive(true);
        }
        if (EventSystem.current.currentSelectedGameObject.name.Equals("ResumeTrainButton"))
        {
            GameControl.instance.confirmations[3].SetActive(false);
            Time.timeScale = 2f;
        }
    }

    public void ShowButtons(bool show)
    {
        foreach (GameObject button in GameControl.instance.buttons)
        {
            button.gameObject.SetActive(show);
        }
    }

    public void ShowDifficulty(bool show)
    {
        foreach (GameObject button in GameControl.instance.difficultyButtons)
        {
            button.gameObject.SetActive(show);
        }
        foreach (GameObject button in GameControl.instance.menuButtons)
        {
            button.gameObject.SetActive(!show);
        }
        if (EventSystem.current.currentSelectedGameObject.name.Equals("BackButton")) {
            GameControl.instance.noCustomPanel.SetActive(false);
        }
    }
}
