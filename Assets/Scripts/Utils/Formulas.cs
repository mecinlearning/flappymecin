﻿using System;

public static class Formulas {
    private static readonly Random random = new Random();

    public static double Sigmoid(double x) {
        return 1 / (1 + Math.Exp(-x));
    }

    public static double DerivativeSigmoid(double x) {
        return x * (1 - x);
    }

    public static double RandomStandardNormal() {
        double uniform1 = 1.0 - random.NextDouble();
        double uniform2 = 1.0 - random.NextDouble();
        return Math.Sqrt(-2.0 * Math.Log(uniform1))
            * Math.Sin(2.0 * Math.PI * uniform2);
    }

    public static double RandomRangeNegativeOneAndOne() {
        return random.NextDouble() * 2 - 1;
    }
}
