﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Json : MonoBehaviour {
    private string customBrainPath;
    public string customBrainName;

    public void Start() {
        customBrainPath = Path.Combine(
            Application.persistentDataPath, customBrainName
        );
    }

    public void SaveCustomBrainJson() {
        using (StreamWriter streamWriter = File.CreateText(customBrainPath)) {
            streamWriter.Write(GetBestBrainJson());
        }
    }

    public static string GetBestBrainJson() {
        MecinAI best = AIPool.getMecins()[0].GetComponent<MecinAI>();
        ulong maxScore = best.GetScore();
        foreach (GameObject mecin in AIPool.getMecins()) {
            MecinAI current = mecin.GetComponent<MecinAI>();
            if (current.GetScore() > maxScore) {
                best = current;
            }
        }
        return best.GetBrain().ToJson();
    }
}
