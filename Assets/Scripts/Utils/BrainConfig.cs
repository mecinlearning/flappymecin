﻿using System.IO;
using UnityEngine;

public static class BrainConfig {
    private static string brainPath;
    private static string[] brainName = { "CustomBrain", "EasyBrain", "MediumBrain", "HardBrain" };
    private static int brainSelection = 0;

    public static void SetBrainSelection(int selection) {
        brainSelection = selection < 0 || selection > 3 ? 0 : selection;
    }

    public static string GetBrainPath() {
        return GetBrainPath(brainSelection);
    }

    public static string GetBrainPath(int selection) {
        return Path.Combine(
            Application.persistentDataPath, brainName[selection] + ".json"
        );
    }

    public static NeuralNetwork GetBrainFromJson() {
        string jsonBrain;
        if (brainSelection == 0) {
            using (StreamReader streamReader = File.OpenText(GetBrainPath())) {
                jsonBrain = streamReader.ReadToEnd();
            }
        } else {
            jsonBrain = Resources.Load<TextAsset>("Brains/" + brainName[brainSelection]).text;
        }
        return Newtonsoft.Json.JsonConvert.DeserializeObject<NeuralNetwork>(jsonBrain);
    }
}
