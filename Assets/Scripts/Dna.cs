﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Dna : MonoBehaviour {
    private bool triggered;

    //If the player hits the trigger collider in between the columns then the player scored
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Mecin>() || other.GetComponent<MecinAI>())
        {
            if (SceneManager.GetActiveScene().name.Equals("Train Scene"))
            {
                other.gameObject.GetComponent<MecinAI>().AddScore();
            }
            if (!triggered) {
                GameControl.instance.MecinScored();
                triggered = true;
            }
        }
    }

    public void SetTriggered(bool value) {
        triggered = value;
    }

    public bool IsTriggered() {
        return triggered;
    }
}
