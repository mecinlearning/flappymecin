﻿using UnityEngine;

public class Mecin : MonoBehaviour
{
    private bool isDead = false;
    private Rigidbody2D rb2d;
    private float upVelocity = 4f;
    public AudioClip shakeSound;
    public AudioClip crashSound;
    private AudioSource source;
    
    void Start()
    {
        Time.timeScale = 1.5f;
        rb2d = GetComponent<Rigidbody2D>();
        source = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        if (!isDead)
        {
            if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space)) && Time.timeScale != 0)
            {
                rb2d.velocity = new Vector2(0, upVelocity); // Mecin jumps
                source.PlayOneShot(shakeSound, 1f);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        //If mecin collides with MecinAI, ignore it.
        if (collision.collider.tag.Equals("MecinAI"))
        {
            Physics2D.IgnoreCollision(collision.collider, collision.otherCollider);
        }
        // If mecin collides with dna, player is dead.
        else
        {
            if (!isDead)
            {
                source.PlayOneShot(crashSound, 1f);
                rb2d.velocity = Vector2.zero;
                isDead = true;
                GameControl.instance.MecinDied();
            }
        }
    }
}
