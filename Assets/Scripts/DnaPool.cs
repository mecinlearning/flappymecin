﻿using UnityEngine;

public class DnaPool : MonoBehaviour {
    public GameObject dnaPrefab;                                 //The dna game object.
    public int dnaPoolSize = 2;                                  //How many dnas to keep on standby.
    public float spawnRate = 3.5f;                               //How quickly dnas spawn.
    public float dnaMin = -1.25f;                                //Minimum y value of the dna position.
    public float dnaMax = 3.25f;                                 //Maximum y value of the dna position.

    private static GameObject[] dnas;                            //Collection of pooled dnas.
    private static int currentDna = 0;                           //Index of the current dna in the collection.
    
    private float spawnXPosition = 5f;
    private float timeSinceLastSpawned;


    void Start() {
        timeSinceLastSpawned = 0f;
        Vector2 objectPoolPosition = new Vector2(spawnXPosition, Random.Range(dnaMin, dnaMax));
        dnas = new GameObject[dnaPoolSize];
        for (int i = 0; i < dnaPoolSize; i++) {
            dnas[i] = (GameObject)Instantiate(dnaPrefab, objectPoolPosition, Quaternion.identity);
            dnas[i].GetComponent<Dna>().SetTriggered(true);
        }
        dnas[0].GetComponent<Dna>().SetTriggered(false);
    }


    //Spawns dnas as long as the game is not over.
    void Update() {
        timeSinceLastSpawned += Time.deltaTime;

        if (GameControl.instance.gameOver == false && timeSinceLastSpawned >= spawnRate) {
            timeSinceLastSpawned = 0f;

            //Set a random y position for the dna
            float spawnYPosition = Random.Range(dnaMin, dnaMax);
            dnas[currentDna].transform.position = new Vector2(spawnXPosition, spawnYPosition);
            dnas[currentDna].GetComponent<Dna>().SetTriggered(false);
            currentDna++;

            if (currentDna >= dnaPoolSize) {
                currentDna = 0;
            }
        }
    }

    public static GameObject GetCurrentDna()
    {
        if (currentDna > 0)
        {
            return dnas[0];
        }
        return dnas[1];
    }
}
