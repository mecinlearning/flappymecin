﻿using UnityEngine;

public class MecinMenu : MonoBehaviour
{
    private Rigidbody2D rb2d;
    public float minVelocity = 2f;
    public float maxVelocity = 5f;
    public float minHeight = 1f;
    public float spinChance = 0.3f;
    public float spinTorque = -50f;
    public float timeScale = 1.5f;
    private float rotationCheck = 3;
    private bool hasSpinned;

    // Start is called before the first frame update
    void Start() {
        Time.timeScale = timeScale;
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        if (transform.position.y < minHeight) {
            if (hasSpinned) {
                if (rb2d.rotation < rotationCheck|| rb2d.rotation > -rotationCheck) {
                    rb2d.rotation = 0;
                    rb2d.angularVelocity = 0;
                    hasSpinned = false;
                }
            }
            rb2d.velocity = new Vector2(0, Random.Range(minVelocity, maxVelocity));
            if (Random.Range(0f, 1f) <= spinChance && rb2d.angularVelocity == 0) {
                rb2d.AddTorque(spinTorque);
                hasSpinned = true;
            }
        }
    }
}
