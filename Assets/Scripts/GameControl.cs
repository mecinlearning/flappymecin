﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour {
    public static GameControl instance;         //A reference to our game control script so we can access it statically.
    public GameObject ai;

    public Text scoreText;                      //A reference to the UI text component that displays the player's score.
    public Text finalScore;
    public Text generationText;

    public GameObject scorePanel;
    public GameObject pauseButton;
    public GameObject noCustomPanel;

    public GameObject[] buttons;
    public GameObject[] confirmations;
    public GameObject[] menuButtons;
    public GameObject[] difficultyButtons;

    private int playerScore = 0;                //The player's score.
    private string activeSceneName;
    public bool gameOver = false;               //Is the game over?
    public float scrollSpeed = -1.5f;

    public AudioClip scoreSound;
    public AudioClip winSound;
    public AudioClip loseSound;
    private AudioSource source;


    void Awake() {
        Screen.SetResolution(564, 960, false);
        Screen.fullScreen = false;
        source = GetComponent<AudioSource>();

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        activeSceneName = SceneManager.GetActiveScene().name;
        if (activeSceneName.Equals("VS Scene") || activeSceneName.Equals("Train Scene"))
        {
            buttons = GameObject.FindGameObjectsWithTag("PauseMenu");
            foreach (GameObject button in buttons)
            {
                button.gameObject.SetActive(false);
            }

            confirmations = new GameObject[4];
            confirmations[0] = GameObject.Find("RestartConfirmation");
            confirmations[1] = GameObject.Find("MenuConfirmation");

            if (activeSceneName.Equals("Train Scene")) {
                confirmations[2] = GameObject.Find("SaveAIConfirmation");
                confirmations[3] = GameObject.Find("SaveSuccess");
            }
            if (activeSceneName.Equals("VS Scene"))
            {
                finalScore.enabled = false;
                confirmations[2] = GameObject.Find("YouWin");
                confirmations[3] = GameObject.Find("YouLose");
            }
            foreach (GameObject confirmation in confirmations)
            {
                if (confirmation != null) {
                    confirmation.gameObject.SetActive(false);
                }
            }
        }

        if (activeSceneName.Equals("Main Menu"))
        {
            menuButtons = GameObject.FindGameObjectsWithTag("MainMenuButton");
            foreach (GameObject button in menuButtons)
            {
                button.gameObject.SetActive(true);
            }

            difficultyButtons = GameObject.FindGameObjectsWithTag("DifficultyButton");
            foreach (GameObject button in difficultyButtons)
            {
                button.gameObject.SetActive(false);
            }
            source.Play();
        }
    }

    public void MecinScored() {
        if (gameOver)
            return;
        playerScore++;
        string prefix = activeSceneName == "Train Scene" ? "Max Score: " : "Score: ";
        scoreText.text = prefix + playerScore.ToString();
        source.PlayOneShot(scoreSound, 1f);
    }

    public void MecinDied() {
        gameOver = true;
        pauseButton.SetActive(false);
        finalScore.text = playerScore.ToString();
        scorePanel.SetActive(false);
        finalScore.enabled = true;
        if (ai.GetComponent<MecinAI>().IsDead())
        {
            source.PlayOneShot(winSound, 0.3f);
            this.confirmations[2].SetActive(true);
        }
        else
        {
            source.PlayOneShot(loseSound, 8f);
            this.confirmations[3].SetActive(true);
        }
    }

    public void UpdateGenerationText(int generation)
    {
        generationText.text = "Generation: " + generation;
    }
}
