﻿using System;

public class NeuralNetwork {
    private static readonly Random random = new Random();
    private readonly int nInput, nHidden, nOutput;
    public Matrix weightsInputHidden, weightsHiddenOutput, biasHidden, biasOutput;

    /*
     * Empty constructor for deserialization purposes.
     */
    public NeuralNetwork() {}

    /*
     * Constructor with number of input, hidden, and output nodes.
     */
    public NeuralNetwork(int nInput, int nHidden, int nOutput) {
        this.nInput = nInput;
        this.nHidden = nHidden;
        this.nOutput = nOutput;

        this.weightsInputHidden = new Matrix(this.nHidden, this.nInput);
        this.weightsHiddenOutput = new Matrix(this.nOutput, this.nHidden);
        this.biasHidden = new Matrix(this.nHidden, 1);
        this.biasOutput = new Matrix(this.nOutput, 1);

        this.weightsInputHidden.Randomize();
        this.weightsHiddenOutput.Randomize();
        this.biasHidden.Randomize();
        this.biasOutput.Randomize();
    }

    /*
     * Constructor with an existing NeuralNetwork.
     */
    public NeuralNetwork(NeuralNetwork other) {
        this.nInput = other.nInput;
        this.nHidden = other.nHidden;
        this.nOutput = other.nOutput;

        this.weightsInputHidden = other.weightsInputHidden.Copy();
        this.weightsHiddenOutput = other.weightsHiddenOutput.Copy();
        this.biasHidden = other.biasHidden.Copy();
        this.biasOutput = other.biasOutput.Copy();
    }

    /*
     * Return a copy of this NeuralNetwork.
     */
    public NeuralNetwork Copy() {
        return new NeuralNetwork(this);
    }

    /*
     * Compute the output based on the inputs.
     */
    public double[] Compute(double[] input) {
        Matrix inputs = Matrix.FromArray(input);
        Matrix hidden = Matrix.Multiply(this.weightsInputHidden, inputs);
        hidden.Add(this.biasHidden);
        this.Activate(hidden);

        Matrix output = Matrix.Multiply(this.weightsHiddenOutput, hidden);
        output.Add(this.biasOutput);
        this.Activate(output);

        return output.ToArray();
    }

    public void Activate(Matrix m) {
        m.Delegate(Formulas.Sigmoid);
    }

    public void Mutate(double rate) {
        double MutateHelper(double value) {
            if (random.NextDouble() < rate) {
                return value + Formulas.RandomStandardNormal();
            }
            return value;
        }
        Matrix[] mutationTarget = {
            this.weightsInputHidden, this.weightsHiddenOutput,
            this.biasHidden, this.biasOutput
        };
        foreach (Matrix m in mutationTarget) {
            m.Delegate(MutateHelper);
        }
    }

    public string ToJson() {
        return Newtonsoft.Json.JsonConvert.SerializeObject(this);
    }

}
