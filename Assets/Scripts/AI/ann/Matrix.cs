﻿using System;

public class Matrix {
    private static readonly string B_MUST_MATCH_ROWS_AND_COLS =
        "Rows and columns of b should match those of this Matrix.";
    private static readonly string A_COLS_MUST_MATCH_B_ROWS =
        "Columns of a must match rows of b.";
    public int rows, cols;
    public double[,] data;
    public delegate double ModifyElement(double elementValue);
    public delegate double DoubleValue();

    /*
     * Constructor.
     */
    public Matrix(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.data = new double[rows, cols];
    }

    /*
     * Return a copy of this Matrix.
     */
    public Matrix Copy() {
        Matrix newMatrix = new Matrix(this.rows, this.cols);
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                newMatrix.data[i, j] = this.data[i, j];
            }
        }
        return newMatrix;
    }

    /*
     * Modify each element in the Matrix using a delegate function
     * based on its original value.
     */
    public void Delegate(ModifyElement delegateFunction) {
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                this.data[i,j] = delegateFunction(this.data[i, j]);
            }
        }
    }

    /*
     * Modify each element in the Matrix using a delegate function.
     */
    public void Delegate(DoubleValue delegateFunction) {
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                this.data[i, j] = delegateFunction();
            }
        }
    }

    /*
     * Return a column Matrix from an array.
     */
    public static Matrix FromArray(double[] arr) {
        Matrix newMatrix = new Matrix(arr.GetLength(0), 1);
        for (int i = 0; i < arr.GetLength(0); i++) {
            newMatrix.data[i, 0] = arr[i];
        }
        return newMatrix;
    }

    /*
     * Return an array representation of this Matrix.
     */
    public double[] ToArray() {
        double[] arr = new double[this.rows * this.cols];
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                arr[i * this.cols + j] = this.data[i, j];
            }
        }
        return arr;
    }

    /*
     * Fill the data with random values between -1 and 1.
     */
    public void Randomize() {
        Delegate(Formulas.RandomRangeNegativeOneAndOne);
    }

    /*
     * Return a transposed version of Matrix m.
     */
    public static Matrix Transpose(Matrix m) {
        Matrix newMatrix = new Matrix(m.cols, m.rows);
        for (int i = 0; i < m.cols; i++) {
            for (int j = 0; j < m.rows; j++) {
                newMatrix.data[i, j] = m.data[j, i];
            }
        }
        return newMatrix;
    }

    /*
     * Add this Matrix with Matrix b.
     */
    public void Add(Matrix b) {
        if (this.rows != b.rows || this.cols != b.cols) {
            Console.WriteLine(B_MUST_MATCH_ROWS_AND_COLS);
            return;
        }
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                this.data[i, j] += b.data[i, j];
            }
        }
    }

    /*
     * Add n to every element in this Matrix.
     */
    public void Add(double n) {
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                this.data[i, j] += n;
            }
        }
    }

    /*
     * Subtract this Matrix with Matrix b.
     */
    public void Subtract(Matrix b) {
        Matrix minusB = b.Copy();
        b.Multiply(-1);
        Add(b);
    }

    /*
     * Return the result of matrix multiplication of a and b.
     */
    public static Matrix Multiply(Matrix a, Matrix b) {
        if (a.cols != b.rows) {
            Console.WriteLine(A_COLS_MUST_MATCH_B_ROWS);
            return null;
        }

        Matrix result = new Matrix(a.rows, b.cols);
        for (int i = 0; i < a.rows; i++) {
            for (int j = 0; j < b.cols; j++) {
                double sum = 0;
                for (int k = 0; k < a.cols; k++) {
                    sum += a.data[i, k] * b.data[k, j];
                }
                result.data[i, j] = sum;
            }
        }
        return result;
    }

    /*
     * Multiply every element in the data by n.
     */
    public void Multiply(double n) {
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                this.data[i, j] *= n;
            }
        }
    }

    /*
     * Multiply this Matrix with Matrix b element-wise. (Hadamard product)
     */
    public void Multiply(Matrix b) {
        if (this.rows != b.rows || this.cols != b.cols) {
            Console.WriteLine(B_MUST_MATCH_ROWS_AND_COLS);
            return;
        }
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                this.data[i, j] *= b.data[i, j];
            }
        }
    }

    /*
     * Return the string representation of this Matrix.
     */
    public override string ToString() {
        string result = "";
        for (int i = 0; i < this.rows; i++) {
            if (i > 0) {
                result += "\n";
            }
            for (int j = 0; j < this.cols; j++) {
                if (j > 0) {
                    result += "\t";
                }
                result += string.Format("{0:0.00}", this.data[i, j]);
            }
        }
        return result;
    }

}
