using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AIPool : MonoBehaviour {
    public static int TOTAL_MECIN = 30;
    private static List<GameObject> mecins;
    public GameObject aiPrefab;

    void Start()
    {
        Time.timeScale = 2f;
        mecins = new List<GameObject>();
        for (int i = 0; i <= TOTAL_MECIN - 1; i++)
        {
            mecins.Add((GameObject)Instantiate(aiPrefab, new Vector2(0, 0), Quaternion.identity));
        }
        GeneticAlgorithm.NextGeneration();
    }

    private void Update()
    {
        if (mecins.Count == 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public static List<GameObject> getMecins()
    {
        return mecins;
    }
}