﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MecinAI : MonoBehaviour
{
    private bool isDead = false;
    private Rigidbody2D rb2d;
    public float upVelocity = 4f;
    private NeuralNetwork brain;
    private ulong score;

    private float screenHeight;
    private float screenWidth;

    private void Awake() {
        this.rb2d = GetComponent<Rigidbody2D>();
        this.brain = new NeuralNetwork(5, 10, 2);
        this.score = 0;
        RectTransform rectangle = GameObject.FindGameObjectWithTag("GameSpace").GetComponent<RectTransform>();
        this.screenWidth = rectangle.rect.width;
        this.screenHeight = rectangle.rect.height;
    }
    
    void Update()
    {
        if (!this.isDead)
        {
            Calculate();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if ((collision.collider.tag.Equals("MecinAI")) || (collision.collider.tag.Equals("MecinPlayer")))
        {
            Physics2D.IgnoreCollision(collision.collider, collision.otherCollider);
        }
        else
        {
            rb2d.velocity = Vector2.zero;
            if (String.Equals(SceneManager.GetActiveScene().name, "Train Scene"))
            {
                AIProperties.savedProperties.Add(new AIProperties(this.brain, this.score));
                AIPool.getMecins().Remove(this.gameObject);
            }
            this.isDead = true;
            this.gameObject.SetActive(false);
        }
    }

    private void Jump()
    {
        this.rb2d.velocity = new Vector2(0, upVelocity);
    }

    //To check whether or not AI should jump
    private void Calculate()
    {
        double[] inputs = new double[5];    //AI neurons

        GameObject dna = DnaPool.GetCurrentDna();
        EdgeCollider2D topDna = dna.transform.GetChild(0).GetComponentInChildren<EdgeCollider2D>();
        EdgeCollider2D bottomDna = dna.transform.GetChild(1).GetComponentInChildren<EdgeCollider2D>();
        inputs[0] = (this.rb2d.transform.position.y - 0.6f) / (screenHeight / 2);                           // Mecin's y-position
        inputs[1] = (topDna.transform.position.y + topDna.offset.y - 1.4f) / (screenHeight / 2);            // Top DNA's end y-position
        inputs[2] = (bottomDna.transform.position.y + bottomDna.offset.y - 0.6f) / (screenHeight / 2);      // Bottom DNA's end y-position
        inputs[3] = dna.transform.position.x / (screenWidth - 1f);                                          // DNA's x-position
        inputs[4] = this.rb2d.velocity.y / (screenHeight);                                                  // Mecin's y-velocity

        double[] output = this.brain.Compute(inputs);
        if (output[0] > output[1])
        {
            this.Jump();
        }
    }

    public ulong GetScore()
    {
        return this.score;
    }

    public void SetBrain(NeuralNetwork brain)
    {
        this.brain = brain;
    }

    public NeuralNetwork GetBrain()
    {
        return this.brain;
    }

    public void AddScore()
    {
        score++;
    }

    public void MutateAI(double rate)
    {
        this.brain.Mutate(rate);
    }

    public Boolean IsDead()
    {
        return this.isDead;
    }
}