﻿using System.Collections.Generic;
using UnityEngine;

public class AIProperties
{
    public static List<AIProperties> savedProperties = new List<AIProperties>();
    public static int generation = 1;
    public NeuralNetwork brain;
    public ulong score;
    public double fitness;

    public AIProperties(NeuralNetwork brain, ulong score)
    {
        this.brain = brain;
        this.score = score;
    }
}