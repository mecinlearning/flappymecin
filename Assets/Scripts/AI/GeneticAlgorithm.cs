﻿public class GeneticAlgorithm
{
    public static void NextGeneration()
    {
        GameControl.instance.UpdateGenerationText(AIProperties.generation);
        AIProperties.generation++;
        if (AIProperties.savedProperties.Count == 0)    //If this is the first generation, don't do selection
        {
            return;
        }
        Selection();
    }

    public static void Selection()
    {
        //Sort brain in descending order by score (AI score is fitness)
        AIProperties.savedProperties.Sort((y, x) => x.score.CompareTo(y.score));
        for (int j = 0; j < AIPool.getMecins().Count; j++)
        {
            //Set a quarter of AI population brains with a quarter from sorted brains
            if (j < AIPool.getMecins().Count / 4)
            {
                MecinAI newMecin = AIPool.getMecins()[j].GetComponent<MecinAI>();
                newMecin.SetBrain(AIProperties.savedProperties[j].brain);
            }
            //Randomize the rest
            else
            {
                System.Random random = new System.Random();
                int index = random.Next(0, AIProperties.savedProperties.Count);
                MecinAI newMecin = AIPool.getMecins()[j].GetComponent<MecinAI>();
                NeuralNetwork newBrain = AIProperties.savedProperties[index].brain.Copy();
                newMecin.SetBrain(newBrain);

                //Mutate the brain
                double rate = random.NextDouble();
                newMecin.MutateAI(rate);
            }
        }
    }
}
