﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BrainLoader : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<MecinAI>().SetBrain(BrainConfig.GetBrainFromJson());
    }

    private void Awake() {
    }

    // Update is called once per frame
    void Update()
    {

    }
}
